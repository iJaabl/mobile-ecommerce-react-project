import React from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import NavBar from "./components/NavBar";
import Routes from "./Routes";
import Modal from "./components/Modal";
import useModal from "./useModal";

export default () => {
  const [modal] = useModal();
  return (
    <>
      <NavBar />
      <Routes />
      {modal.isOpen ? <Modal /> : null}
    </>
  );
};
