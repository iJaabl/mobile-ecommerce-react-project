import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { Provider } from "./context";
import * as initState from "./data";

const MobileAF = () => (
    <Provider init={initState}>
      <App />
    </Provider>
  );

ReactDOM.render(<MobileAF />, document.getElementById("root"));
