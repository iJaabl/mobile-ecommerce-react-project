import styled from "styled-components";

export const ButtonStyle = styled.button`
  font-size: 1.4rem;
  border: 0.2rem solid var(--mainOrange);
  border-radius: 10px;
  padding: 0.2rem 0.5rem;
  margin: 0.2rem 0.5rem;
  cursor: pointer;
  transition: all 0.1s ease-in-out;
  background: ${props =>
    props.callToAction ? `var(--accentRed)` : `transparent`};
  border-color: ${props =>
    props.callToAction ? `var(--accentRed)` : `var(--mainOrange)`};
  color: ${props =>
    props.callToAction ? `var(--mainLight)` : `var(--lightBlue)`};
  &:hover {
    box-shadow: 2px 2px 5px 0px rgba(0, 0, 0, 0.2);
    background: ${props =>
      props.callToAction ? `var(--accentRed)` : `var(--mainOrange)`};
    color: var(--mainDark);
  }
  &:focus {
    outline: none;
  }
`;

export const NavStyle = styled.nav`
  background: var(--pitchBlack);
  .nav-link {
    color: var(--mainLight) !important;
    font-size: 1.3rem;
    text-transform: capitalize;
  }
`;
