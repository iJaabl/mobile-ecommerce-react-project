import React from "react";
import ProductCard from "./ProductCard";
import Title from "./Title";
import useInventory from "../useInventory";

export default () => {
  const [inventory] = useInventory();
  const { storeProducts } = inventory;

  return (
    <>
      <div className="py-5">
        <div className="container">
          <Title name="What's in" title="stock" />
          <div className="fix">
            {storeProducts.map(product => {
              let { id } = product;
              return <ProductCard key={id} product={product} />;
            })}
          </div>
        </div>
      </div>
    </>
  );
};
