import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import useHelper from "../useHelper";

export default function ProductCard({ product }) {
  const [handleDetail, addToCart, openModal] = useHelper();
  const { id, title, img, inCart, price } = product;

  return (
    <>
      <CardStyle className="col-9 mx-auto col-md-6 col-lg-3 my-3">
        <div className="card" onClick={() => handleDetail(id)}>
          <div className="img-container p-5">
            <Link to="/details">
              <img src={img} alt="product" className="card-img-top" />
            </Link>
            <button
              className="cart-btn"
              onClick={() => {
                addToCart(id);
                openModal(id);
              }}
            >
              {inCart ? (
                <p className="mb-0">In Cart</p>
              ) : (
                <i className="fas fa-cart-plus" />
              )}
            </button>
          </div>
          <footer className="card-footer d-flex justify-content-between">
            <Link to="/details">
              <p className="align-self-center mb-0">{title}</p>
            </Link>
            <h5 className="text-blue font-italic mb-0">
              <span className="mr-1">$</span>
              {price}
            </h5>
          </footer>
        </div>
      </CardStyle>
    </>
  );
}

ProductCard.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.number,
    img: PropTypes.string,
    title: PropTypes.string,
    price: PropTypes.number,
    inCart: PropTypes.bool
  })
};

const CardStyle = styled.div`
  .card {
    border-color: transparent;
    transition: all 1s linear;
  }
  .card-footer {
    background: transparent;
    border-top: transparent;
    transition: all 0.5s linear;
  }
  .img-container {
    position: relative;
    overflow: hidden;
  }
  .card-img-top {
    transition: all 0.7s linear;
  }
  .cart-btn {
    position: absolute;
    bottom: 0;
    right: 0;
    padding: 0.2rem 0.4rem;
    background: var(--lightBlue);
    color: var(--mainLight);
    border: none;
    font-size: 1.4rem;
    border-radius: 0.5rem 0 0 0;
    transform: translate(100%, 100%);
    transition: all 0.5s linear;
  }
  .img-container:hover .card-img-top {
    transform: scale(1.2);
  }
  .img-container:hover .cart-btn {
    transform: translate(0, 0);
  }
  .cart-btn:hover {
    color: var(--mainBlue);
    background: var(--mainLight);
    cursor: pointer;
  }
  &:hover {
    .card {
      border: 0.1rem solid rgba(0, 0, 0, 0.2);
      box-shadow: 2px 2px 5px 0px rgba(0, 0, 0, 0.2);
    }
    .card-footer {
      background: rgba(0, 0, 0, 0.3);
      font-decoration: none;
    }
  }
  &:focus {
    outline: none;
  }
`;
