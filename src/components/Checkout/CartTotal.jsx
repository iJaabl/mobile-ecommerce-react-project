import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import PayPalButton from "./PayPalButton";
import useHelper from "../../useHelper";

export default function CartTotal({ cart, history }) {
  const [, , , , , , , clearCart] = useHelper();
  const [amountDue, setAmountDue] = useState({ subtotal: 0, tax: 0, total: 0 });
  const { subtotal, tax, total } = amountDue;

  const calculateAmountDue = () => {
    let subtotal = 0;
    cart.map(product => (subtotal += product.total));
    const calcTax = subtotal * 0.08;
    const tax = parseFloat(calcTax.toFixed(2));
    const total = subtotal + tax;
    setAmountDue({ subtotal, tax, total });
  };

  useEffect(() => calculateAmountDue(), [cart]);

  return (
    <>
      <div className="container">
        <div className="row">
          <div className="col-10 mt-2 ml-md-auto col-sm-8 text-capitalize text-right">
            <Link to="/">
              <button
                className="btn btn-outline-danger text-uppercase mb-5 px-5"
                type="button"
                onClick={() => clearCart()}
              >
                clear cart
              </button>
            </Link>
            <h5>
              <span className="text-title">subtotal : </span>
              <strong>${subtotal}</strong>
            </h5>
            <h5>
              <span className="text-title">tax : </span>
              <strong>${tax}</strong>
            </h5>
            <h5>
              <span className="text-title">total : </span>
              <strong>${total}</strong>
            </h5>
            <PayPalButton
              total={total}
              clearCart={clearCart}
              history={history}
            />
          </div>
        </div>
      </div>
    </>
  );
}
