import React from "react";
import { Link } from "react-router-dom";
import { ButtonStyle, NavStyle } from "./styles";
import useCart from "../useCart";

export default () => {
  const [cart] = useCart();
  return (
    <NavStyle className="navbar navbar-expand-sm navbar-dark px-sm-5">
      <Link to="/">
        <img
          src="https://i.imgur.com/Kn33QVs.png"
          alt="logo"
          width="200px"
          className="navbar-brand"
        />
      </Link>
      <ul className="navbar-nav align-items-center">
        <li className="nav-item ml-5">
          <Link to="/" className="nav-link">
            products
          </Link>
        </li>
      </ul>
      <Link to="/cart" className="ml-auto">
        <ButtonStyle>
          <span className="mr-2">
            <i className="fas fa-shopping-cart" />
          </span>
          Cart {cart.length === 0 ? null : `${cart.length}`}
        </ButtonStyle>
      </Link>
    </NavStyle>
  );
};
