import { useContext, useEffect } from "react";
import { InventoryContext, SetInventoryContext } from "./context";

export default () => {
  const inventory = useContext(InventoryContext);
  const setInventory = useContext(SetInventoryContext);

  useEffect(() => {
    if (inventory === undefined) {
      throw new Error("Don't know what's in the inventory...");
    }

    if (setInventory === undefined) {
      throw new Error("Nothing in setInventory...");
    }
  }, [inventory, setInventory]);

  return [inventory, setInventory];
};
